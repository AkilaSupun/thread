package jav.base.pkg;

import java.net.ServerSocket;
import java.net.Socket;

public class Server {
  private static int portNumber = 1598;

  public static void main(String[] args) {
    try (ServerSocket serverSocket = new ServerSocket(portNumber);) {
      while (true) {
        Socket client = serverSocket.accept();
        new ServerHandler(client).start();
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}

