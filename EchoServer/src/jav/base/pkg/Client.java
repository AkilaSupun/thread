package jav.base.pkg;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Client {
  public static void main(String[] args) {
    int portNumber = 1598;
    String hostName = "127.0.0.1";

    try (Socket serverSocket = new Socket(hostName, portNumber);
        PrintWriter out = new PrintWriter(serverSocket.getOutputStream(), true);
        BufferedReader in =
            new BufferedReader(new InputStreamReader(serverSocket.getInputStream()));
        BufferedReader clientConsole = new BufferedReader(new InputStreamReader(System.in))) {
      String userInput = null;
      while ((userInput = clientConsole.readLine()) != null) {
        out.println(userInput);
        System.out.println("Server Says:" + in.readLine());
      }

    } catch (Exception e) {
      e.printStackTrace();
      System.exit(1);
    }
  }

  public class Hello {

  }
}
