package jav.base.pkg;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ServerHandler extends Thread {
  private Socket clientSocket;
  private PrintWriter out;
  private BufferedReader in;

  public ServerHandler(Socket client) throws IOException {
    this.clientSocket = client;
    this.out = new PrintWriter(clientSocket.getOutputStream(), true);
    this.in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
  }

  @Override
  public void run() {
    try {
      String clientMessage = null;
      while ((clientMessage = this.in.readLine()) != null) {
        out.println(clientMessage.toUpperCase());
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

}
